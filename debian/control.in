Source: gnome-themes-extra
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper (>= 11),
               gnome-pkg-tools (>= 0.10),
               intltool (>= 0.40.0),
               libgtk-3-dev (>= 3.9.12),
               libgtk2.0-dev (>= 2.24.15),
               gtk-update-icon-cache,
               libcairo2-dev,
               libgdk-pixbuf2.0-dev,
               libglib2.0-dev,
               librsvg2-dev
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/gnome-team/gnome-themes-extra
Vcs-Git: https://salsa.debian.org/gnome-team/gnome-themes-extra.git

Package: gnome-themes-extra
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: gnome-themes-extra-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         gtk2-engines-pixbuf
Recommends: gnome-accessibility-themes,
            adwaita-icon-theme (>= 3.18.0-2~)
Breaks: gnome-themes-standard (<= 3.27.90.1)
Replaces: gnome-themes-standard (<= 3.27.90.1)
Description: Adwaita GTK+ 2 theme — engine
 This is the version of Adwaita, the standard GNOME theme, for the
 GTK+ 2.x toolkit.
 .
 The GTK+ 3.x version is already included in libgtk-3-0.
 .
 This package contains the Adwaita theme engine.

Package: gnome-themes-extra-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Conflicts: gnome-themes-standard-data
Replaces: gnome-themes-standard-data
Description: Adwaita GTK+ 2 theme — common files
 This is the version of Adwaita, the standard GNOME theme, for the
 GTK+ 2.x toolkit.
 .
 The GTK+ 3.x version is already included in libgtk-3-0.
 .
 This package contains the common architecture-independent files.

Package: gnome-accessibility-themes
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: High Contrast GTK+ 2 theme and icons
 This is the version of GNOME's standard HighContrast theme, for the
 GTK+ 2.x toolkit. An icon theme is also included.
 .
 The GTK+ 3.x version is already included in libgtk-3-0.

Package: gnome-themes-standard
Architecture: all
Section: oldlibs
Depends: ${misc:Depends},
         gnome-themes-extra
Description: Adwaita GTK+ 2 theme — engine - transitional package
 This package is here to ensure smooth upgrades. It can be removed when
 you see fit.
